# Impostion for printed booklet(s)


This script re-arrange the pages of your document in order to make an imposed sheet layouts for printing. 

It's usefull to make self-printed booklets. You could print two pages per sheet, double-sided. The result could then be folded in half along the width.

- The script is compatible with the use of the [interface polyfill](https://gitlab.pagedmedia.org/tools/interface-polyfill) of paged.js.
- Supports bleeds and crop marks.
- Does not support inch sizes.


## How to use it

Link the script to your document after the paged.js script.

```
<script src="path/to/file/imposition.js"></script>
```

In your CSS, add the custom value bellow to request the application of the script before printing:
```
--paged-layout: booklet;
```

You can also define a range of pages you want to make the booklet. In the following example only page 11 to page 18 will be imposed and printed:
```
--paged-layout: booklet 11 18;
```

The script will only apply when you request printing.



--paged-layout-start: init/print;


## Good to know

- If the total number of pages in your document is not a multiple of 4, blank pages will automatically be added to the imposition.
- If you use only one value for the range of pages (`--paged-layout: booklet 11`), the range go from this value to the last page of your document.
- If the first value of the range of pages is an odd number (page 8), the imposition will automatically start to the previous page (page 7).
- This script works only with paper size and bleed size declared in millimeters (mm) or A0-A10 and B4-B5 page size keywords. Clearly, it doesn't work with inch.
- In order to answer certain problems during the generation of the PDF, the script cuts the pages of 0.1mm on the width. 



## Credits

The MIT License (MIT)

This script is an improved version of [a script](https://gitlab.coko.foundation/pagedjs/hackathon-mars-2021/-/tree/master/imposition_quentin_juhel) developed by Quentin Juhel and Julien Taquet during a Paged.js hackathon. Quentin had themselves reused a script produced by Julien Bidoret.